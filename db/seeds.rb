# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'default_rambles.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  r = Ripple.new
  r.name = row['name']
  r.url = row['url']
  r.message = row['message']
  r.save
  puts "#{r.name}, #{r.url}, #{r.message} saved"
end
puts "There are now #{Ripple.count} rows in the Ripples table"
