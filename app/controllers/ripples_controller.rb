class RipplesController < ApplicationController
  before_action :set_ripple, only: [:show, :update]
  before_action :restrict_destroy_edit, only: [:edit, :destroy]
  
  RIPPLES_PER_PAGE = 10

  # GET /ripples
  # GET /ripples.json
  def index
    session[:page] = 0
    @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)
  end

  # GET /ripples/1
  # GET /ripples/1.json
  def show
  end

  # GET /ripples/new
  def new
    @ripple = Ripple.new
  end

  
  def next
    size = Ripple.all.size
    pages = size / 10
    if (session[:page] > (pages - 1)) 
      @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)      
    else
      session[:page] += 1
      @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)
    end
    
    render :index
  end
  
  def previous
    if (session[:page] == 0) 
      @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)      
    else
      session[:page] -= 1
      @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)
    end
    
    render :index
  end
  
  def oldest
    size = Ripple.all.size
    pages = size / 10  
    session[:page] = pages
    @ripples = Ripple.limit(RIPPLES_PER_PAGE).offset(session[:page] * RIPPLES_PER_PAGE)  
    render :index
  end

  # POST /ripples
  # POST /ripples.json
  def create
    @ripple = Ripple.new(ripple_params)

    respond_to do |format|
      if @ripple.save
        format.html { redirect_to root_url}
        format.json { render :show, status: :created, location: @ripple }
      else
        format.html { render :new }
        format.json { render json: @ripple.errors, status: :unprocessable_entity }
      end

    end

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ripple
      @ripple = Ripple.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ripple_params
      params.require(:ripple).permit(:name, :url, :message)
    end

end
