Rails.application.routes.draw do
  root 'ripples#index'
  
  get '/ripples/oldest', 'ripples#oldest'
  get '/ripples/next', 'ripples#next'
  get '/ripples/previous', 'ripples#previous'
  resources :ripples, :except => [:edit, :destroy]


  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
