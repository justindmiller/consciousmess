require 'test_helper'

class RipplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ripple = ripples(:one)
    @sample = {
      name:   'Xbone',
      url:    'http://www.bing.com',
      message: 'test case for xbone'
    }
  end

  test "should get index" do
    get ripples_url
    assert_response :success
  end

  test "should get new" do
    get new_ripple_url
    assert_response :success
  end

  test "should create ripple" do
    assert_difference('Ripple.count') do
      post ripples_url, params: { ripple: @sample }
    end

    assert_redirected_to root_url
  end

  test "should show ripple" do
    get ripple_url(@ripple)
    assert_response :success
  end
  
  test "should go forward then back to newest" do
    get ripples_url
    get ripples_next_path
    get ripples_path
    assert_equal(0, session[:page])
  end
  
  test "should go to oldest then back 10" do
    get ripples_url
    get ripples_oldest_path
    get ripples_previous_path
    size = Ripple.all.size
    pages = size / 10
    last = pages - 1
    assert_equal(last, session[:page])
  end

end
